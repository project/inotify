# iNotify Realtime Notifications for Drupal

## Contents Of This File

- Introduction
- How It Works
- Requirements
- Installation
- Configuration
- Getting It To Work
- Advanced Usage & Cusomization
- Maintainer

## Introduction

iNotify provides the means to display live notifications based on custom events.

## How It Works

The module provides a custom Notification entity that admins with permissions
can customize its fields and display. Notifications can be created using a
Custom Rules Action that is provided, you can trigger the action on any system
event, and fill the fields with custom data. The module uses NodeJS to
automatically refresh the notifications view whenever it's available on display.
The module also provides a notifications block to place within any region you
want.

*The module does not contain styles, only the basics. You will have to style
the block and write CSS according to your design.*

## Requirements

- [Rules](https://www.drupal.org/project/rules)
- [NodeJS Integration](https://www.drupal.org/project/nodejs)

## Installation

- Open your terminal and download with composer.
  `composer require drupal/inotify`

  OR

- Download the module to your `DRUPAL_ROOT/modules` directory, or where ever you
  install contrib modules on your site.
- Go to `Admin > Extend` and enable the module.

## Configuration

### Enabling Node.js Integration

- Install Node.js on your server, if not installed yet. Download the installer
  and follow the instructions on the Node.js website:
  (https://nodejs.org/en/download/)
- Install the drupal-node.js server application and ensure that the application
  is running.
  Follow the instruction on GitHub: (https://github.com/beejeebus/drupal-nodejs)
- Install and enable the **Node.js integration** Drupal module on your site.
- Visit the Node.js configuration page under the Configuration menu, and enter
  the connection information for your Node.js server application. The protocol,
  port, and service key settings should match the settings you entered in the
  config file of the Node.js server application. The host should be set to the
  hostname of the server on which you installed the Node.js server application.
  This host needs to be publicly accessible (i.e. "localhost" will not work).
- Visit the status page of your Drupal site and ensure that there are no errors.
  Node.js should report that it has successfully connected to the Node.js
  server.

Note: The Drupal module and Node.js server application are sensitive to version
numbers. The Drupal status page will show an error if the versions are not
compatible. For more information about the module, you can visit the
[Node.js Integration Modules Documentation](https://www.drupal.org/node/1713530)

## Getting It To Work

- After installation, make sure you do all the required configurations.
- Go to `admin/config/workflow/rules` and create a new reaction rule based on
  whatever event you want.
- After configuring your reaction rule, make sure to add **Create Notification**
  to your actions list.
  you can add the title and description values within the action. When you are
  done, save you rule.
- Go to your theme block layout and place the **Notifications Block** within the
  intended region (usually the header), then save.
- Now you should see the block displayed in the region.To test it out, you can
  trigger you reaction rule and see the notification block automatically
  refreshing and displaying the new notification.

## Advanced Usage & Cusomization

### Customizing the Notification Entity

- The Notification entity provided is basically like any other content entity
  within Drupal. You can visit `admin/structure/inotify_notification_settings`
  and add or customize fields, alongside changing form display and content
  display modes, just do with it what you do with nodes.

### Customizing the Notification View

- TODO

### Creating a Custom Rules Action

- TODO

### Creating notification programmatically

Just create the inotify_notification entity programmatically:

```
$notification = \Drupal::entityTypeManager()
->getStorage('inotify_notification')
->create({
  'title' => 'Test',
  'description' => 'Test description',
  'target_link' => 'TEST TARGET LINK',
  'uid' => 1, // The user that should recieve this notification.
});
$notification->save();
```

### Overriding Notifications Block Template

For modifing the block template, copy the template file
`templates/block--views-block--notifications-notifications-block.html.twig` to
your custome theme or custom module. You can modify the structure of the block
display, just make sure to put your HTML within the `{% block content %}` tags,
and never forget to use `{{ content }}` to render the actual view.

### Overriding Notifications Entity Template

- For modifing the Notification template, copy the template file
  `templates/inotify-notification.html.twig` to your custome theme or custom
  module. You can modify the structure of the notification display based on the
  variable `view_mode` in case you created a view mode for the entity, you can
  also use the **content** variable just like any other content entity. You can
  always add more variables to the template using the hook
  `template_preprocess_inotify_notification`, go to **inotify.module** file to
  see how it's implemented.

### Custom CSS Styling

The included CSS file only provides a basic style to make it possible to you to
see how the module works. You should have your own CSS file within your theme or
custom module, just style the block like any other element within your theme.


## Maintainers

- Yasser Samman - https://www.drupal.org/u/yasser-samman
  (https://jo.linkedin.com/in/yasseralsamman)
  (y.samman@codersme.com)
- Anas Mawlawi (Anas_maw) - https://www.drupal.org/u/anas_maw