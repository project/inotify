<?php

namespace Drupal\inotify\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;

/**
 * InotifySubscriber.
 */
class InotifySubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DefaultSubscriber object.
   */
  public function __construct(CurrentRouteMatch $current_route_match, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentRouteMatch = $current_route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest'];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    if ($this->currentRouteMatch->getRouteName() == 'entity.inotify_notification.canonical') {
      $notification = $this->currentRouteMatch->getParameter('inotify_notification');
      if ($notification->getStatus() == FALSE) {
        $notification->setStatus(TRUE);
        $notification->save();
      }
      $target = Url::fromUri($notification->getTargetLink());
      $response = new TrustedRedirectResponse($target->toString(), 302);
      $event->setResponse($response);
      return;
    }

    return;
  }

}
