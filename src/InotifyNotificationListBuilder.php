<?php

namespace Drupal\inotify;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines the list builder for rfq.
 */
class InotifyNotificationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = t('#id');
    $header['owner'] = t('Owner');
    $header['title'] = t('Title');
    $header['description'] = t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id']['data'] = [
      '#type' => 'link',
      '#title' => $entity->id(),
    ] + $entity->toUrl()->toRenderArray();

    $row['owner']['data'] = [
      '#type' => 'link',
      '#title' => $entity->getOwner()->getUserName(),
    ] + $entity->getOwner()->toUrl()->toRenderArray();

    $row['title']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
    ] + $entity->toUrl()->toRenderArray();

    $row['description']['data'] = [
      '#type' => 'link',
      '#title' => $entity->get('description')->value,
    ] + $entity->toUrl()->toRenderArray();

    return $row + parent::buildRow($entity);
  }

}
